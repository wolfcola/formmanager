import {
  Directive,
  Input,
  OnInit,
  TemplateRef,
  ViewContainerRef,
} from '@angular/core';
import { Subscription } from 'rxjs';
import { FormManagerService } from '../services/form-manager.service';
import { ShowIf } from '../services/models';


const operatorMap = new Map<string, (a: any, b: any) => boolean>([
  ['==', (a, b) => a == b],
  ['===', (a, b) => a === b],
  ['!=', (a, b) => a != b],
  ['>', (a, b) => a > b],
  ['<', (a, b) => a < b],
  ['>=', (a, b) => a >= b],
  ['<=', (a, b) => a <= b],
  ['default', (a, b) => true],
]);

@Directive({
  selector: '[appShowIf]',
})
export class ShowIfDirective implements OnInit {
  @Input() appShowIf: ShowIf = { id: 'default', operator: '===', value: true };
  private subscription!: Subscription;
  public showComponent = true;
  private hasView = false;

  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef,
    private formManagerService: FormManagerService
  ) {}

  ngOnInit() {
    const { id, operator, value } = this.appShowIf;
    // Subscribe to changes in the field value
    this.subscription = this.formManagerService.fieldData$.subscribe(
      (fieldData) => {
        const field = fieldData.find((f) => f.id === id);
        if (!field && id !== 'default') {
          // Field not found
          console.log(`No field found for ${id}`)
          if (this.hasView) {
            this.viewContainer.clear();
            this.hasView = false;
          }
          return;
        }

        // Get the current field value
        const fieldValue = field ? field.value : 'default';

        // Check the operator
        const operation = operatorMap.get(operator);
        if (!operation) {
          console.warn(`Unsupported operator: ${operator}`);
          if (this.hasView) {
            this.viewContainer.clear();
            this.hasView = false;
          }
          return;
        }

        const showComponent = operation(fieldValue, value);
        this.showComponent = id === 'default' ? true : showComponent;
        // Show or hide the component based on the result
        if (this.showComponent) {
          if (!this.hasView) {
            this.viewContainer.createEmbeddedView(this.templateRef);
            this.hasView = true;
          }
        } else {
          if (this.hasView) {
            this.viewContainer.clear();
            this.hasView = false;
          }
        }
      }
    );
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
