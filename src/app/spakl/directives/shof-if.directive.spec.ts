import { TestBed, ComponentFixture } from '@angular/core/testing';
import { Component, ElementRef, ViewChild } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { FormManagerService } from '../services/form-manager.service';
import { ShowIfDirective } from './shof-if.directive';
import { By } from '@angular/platform-browser';


@Component({
  template: `
    <div *appShowIf="showIf" #container>
      <p>Some content</p>
    </div>
  `
})
class TestComponent {
  showIf = { id: 'field1', operator: '===', value: 42 };
  @ViewChild('container',{read: 'ShowIfDirective'})
  container!: ShowIfDirective;
}

describe('ShowIfDirective', () => {
  let component: TestComponent;
  let fixture: ComponentFixture<TestComponent>;
  let formManagerService: FormManagerService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ShowIfDirective, TestComponent],
      providers: [FormManagerService]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestComponent);
    component = fixture.componentInstance;
    formManagerService = TestBed.inject(FormManagerService);
    fixture.detectChanges();
  });

  it('should hide the component if the field value does not match', async () => {
    formManagerService.updateFieldData('field1', 0);
    fixture.detectChanges();
    expect(component.container.showComponent).toBe(false);
  });

  it('should show the component if the field value matches', () => {
    formManagerService.updateFieldData('field1',42 );
    fixture.detectChanges();
    const containerEl = fixture.debugElement.query(By.directive(ShowIfDirective)).nativeElement;
    expect(containerEl.style.display).toBe('block');
  });

  it('should hide the component if the operator is unsupported', () => {
    component.showIf.operator = 'unsupported';
    formManagerService.updateFieldData('field1',42 );
    fixture.detectChanges();
    const containerEl = fixture.debugElement.query(By.directive(ShowIfDirective)).nativeElement;
    expect(containerEl.style.display).toBe('none');
  });

});
