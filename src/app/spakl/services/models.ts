import { Observable } from "rxjs";

export interface FieldData {
    id: string;
    value: string;
  }

export  interface QuestionValidator {
    type: string;
    message: string;
    value?: number;
  }

export interface FormOption {
    value: any;
    text: string;
  }
export  interface Question {
    optionsLibraryId(optionsLibraryId: any): unknown;
    id: string;
    component: string;
    text: string;
    analyticId: string;
    validators: QuestionValidator[];
    options:FormOption[],
    optionsId?: string,
    showIf?:ShowIf;
  }

export interface ShowIf {
    id: string;
    operator: string;
    value: any;
}

export  interface Section {
    id: string;
    name: string;
    analyticId: string;
    questions: string[];
  }

export  interface FormConfiguration {
    questions: Question[];
    sections: Section[];
  }

export interface FormState {
    formName: string;
    formLibraryId: string;
    createdBy: string;
    createdTime: string;
    modifiedBy: string;
    lastModifiedTime: string;
    organizationId: string;
    currentSectionIndex: number;
    isCompleted: boolean;
    formConfiguration: FormConfiguration;
    fieldData: FieldData[];
  }

 export interface IFormManager {
    state$: Observable<FormState>;
    state: FormState;
    currentSectionIndex: number;
    currentSection: any;
    sections: any[];
    questions: any[];
    fieldData$: Observable<{id: string, value: any}[]>;
    getFieldDataById(id: string): {id: string, value: any};
    updateFieldData(fieldId: string, newValue: any): void;
    goToNextSection(): void;
    goToPreviousSection(): void;
    goToSection(sectionId: string): void;
    reset(): void;
  }
