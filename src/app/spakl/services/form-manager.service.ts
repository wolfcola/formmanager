import { Injectable } from '@angular/core';
import { FormControl, ValidatorFn, Validators } from '@angular/forms';
import { BehaviorSubject, map, Observable } from 'rxjs';
import { FieldData, FormOption, FormState, IFormManager, Question, QuestionValidator } from './models';

@Injectable({
  providedIn: 'root'
})
export class FormManagerService implements IFormManager {
  private _state: FormState;
  private _state$: BehaviorSubject<any>;

  constructor() {
    this._state = {
      formName: 'init',
      formLibraryId: 'init',
      createdBy: 'alias-init',
      createdTime: '',
      modifiedBy: '',
      lastModifiedTime: '',
      organizationId: '',
      currentSectionIndex: 0,
      isCompleted: false,
      formConfiguration: {
        questions: [],
        sections: []
      },
      fieldData: []
    };
    this._state$ = new BehaviorSubject(this._state);
  }

  get state$(): Observable<any> {
    return this._state$.asObservable();
  }

  get state(): any {
    return this._state$.getValue();
  }

  setState(newState: any) {
    this._state = newState;
    this._state$.next(this._state);
    this.currentSectionIndex = this.currentSectionIndex
  }

  get currentSectionIndex(): number {
    return this.state.currentSectionIndex;
  }

  set currentSectionIndex(index: number) {
    this.state.currentSectionIndex = index;
    this._state$.next(this._state);
  }

  get currentSection(): any {
    return this.state.formConfiguration.sections[this.currentSectionIndex];
  }

  get currentSectionId(): any {
    return this.state.formConfiguration.sections[this.currentSectionIndex].id;
  }

  get sections(): any[] {
    return this.state.formConfiguration.sections;
  }

  get questions(): any[] {
    return this.state.formConfiguration.questions;
  }

  get isCompleted(): any[] {
    return this.state.isCompleted;
  }

  get fieldData$(): Observable<any[]> {
    return this._state$.asObservable().pipe(map((state: FormState )=> state.fieldData));
  }

  getQuestionConfigById(questionId: string): Question {
    return this.state.formConfiguration.questions.find((question: Question) => question.id === questionId);
  }

  getFieldDataById(id: string): any {
    return this.state.fieldData.find((field: FieldData) => field.id === id);
  }

  updateFieldData(fieldId: string, newValue: any): void {
    const fieldIndex = this.state.fieldData.findIndex((field: FieldData) => field.id === fieldId);
    if (fieldIndex !== -1) {
      this.state.fieldData[fieldIndex].value = newValue;
      this._state$.next(this._state);
    } else  {
      this.state.fieldData.push({
        id: fieldId,
        value: newValue
      });
      this._state$.next(this.state);
    }
  }

  goToNextSection(): void {
    if (this.currentSectionIndex < this.sections.length - 1) {
      this.currentSectionIndex++;
    }
  }

  goToPreviousSection(): void {
    if (this.currentSectionIndex > 0) {
      this.currentSectionIndex--;
    }
  }

  goToSection(sectionId: string): void {
    const index = this.sections.findIndex(section => section.id === sectionId);
    if (index !== -1) {
      this.currentSectionIndex = index;
    }
  }

  getOptionsFromLibrary(options: FormOption[], optionsId?: string): FormOption[] {
    if(!optionsId){
      return options
    } else {
      console.log("mock get option")
      return [{value: "test",text: "worked"}]
    }
  }

  getOption(options: FormOption[],text: string){
    const option = options.find(option=>{ return option.text === text})
    if (option){
      return option.value
    }
  }


  getValidators(question: Question): ValidatorFn[] {
    console.log("getting validators for ", question.id )
    console.log(question)
    const validatorsMap = new Map([
      ["required", Validators.required],
      ["minlength", Validators.minLength(this.getOption(question.options, 'minLength') || 8)],
      ["maxlength", Validators.maxLength(this.getOption(question.options, 'maxLength'))|| 8],
      ["pattern", Validators.pattern(this.getOption(question.options, 'pattern')|| '')],
      ["email", Validators.email]
    ]);

    let validators: ValidatorFn[] = [];

    if (question.validators) {
      question.validators.forEach(validator=> {
        const val = validatorsMap.get(validator.type)
        if (val){
          validators.push(val)
        }
      });
    }

    return validators;
  }

  getFormControl(question: Question): FormControl {
    // Create form control with validators
    return new FormControl(this.getFieldDataById(question.id), this.getValidators(question));
  }

  reset(): void {
    this.state.fieldData = [];
    this.currentSectionIndex = 0;
    this._state$.next(this._state);
  }
}
