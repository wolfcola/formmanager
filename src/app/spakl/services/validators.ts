import { FormControl } from "@angular/forms";

export function required(control: FormControl): {[key: string]: any} {
  if (!control.value || control.value === '') {
    return { required: true };
  }
  return null;
}

export function custom(control: FormControl): {[key: string]: any} {
  // custom logic here
  return null;
}
