import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShowIfDirective } from './directives/shof-if.directive';
import { BaseFormComponent } from './components/base/base.component';
import { LoaderComponent } from './components/loader/loader.component';
import { InputComponent } from './components/input/input.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule} from '@angular/material/input'
import { MatFormFieldModule } from '@angular/material/form-field';
import { ValidatormessageComponent } from './components/validatormessage/validatormessage.component';


@NgModule({
  declarations: [
    ShowIfDirective,
    BaseFormComponent,
    LoaderComponent,
    InputComponent,
    ValidatormessageComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MatInputModule,
    MatFormFieldModule
  ],
  exports: [
    ShowIfDirective,
    BaseFormComponent,
    LoaderComponent,
    InputComponent
  ]
})
export class SpaklModule { }
