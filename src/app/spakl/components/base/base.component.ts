import { Component, inject, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { FormManagerService } from '../../services/form-manager.service';
import { FormOption, Question } from '../../services/models';

@Component({
  template: ''
})
export class BaseFormComponent  {
  formManagerService = inject(FormManagerService)
  formName!: string;
  fieldData$!: Observable<any>;
  sectionIndex!: number;
  isCompleted!: boolean;
  questionConfig!: Question
  formControl!: FormControl
  options$: any
  initializeComponenet(){
    this.formName = this.formManagerService.state.formConfiguration.formName;
    this.fieldData$ = this.formManagerService.fieldData$;
    this.sectionIndex = this.formManagerService.currentSectionIndex;
    this.isCompleted = this.formManagerService.state.isCompleted;
    this.options$ = this.formManagerService.getOptionsFromLibrary(this.questionConfig.options, this.questionConfig.optionsId);
  }

  goToNextSection(): void {
    this.formManagerService.goToNextSection();
  }

  goToPreviousSection(): void {
    this.formManagerService.goToPreviousSection();
  }

  goToSection(sectionId: string): void {
    this.formManagerService.goToSection(sectionId);
  }
  setFormControl(){
    this.formControl = this.formManagerService.getFormControl(this.questionConfig);
  }
}
