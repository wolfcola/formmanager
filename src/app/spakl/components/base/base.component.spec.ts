import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BaseFormComponent } from './base-form.component';
import { FormManagerService } from '../../services/form-manager.service';
import { of } from 'rxjs';
import { Question } from '../../services/models';

describe('BaseFormComponent', () => {
  let component: BaseFormComponent;
  let fixture: ComponentFixture<BaseFormComponent>;
  let formManagerServiceSpy: jasmine.SpyObj<FormManagerService>;

  beforeEach(() => {
    const formManagerServiceSpyObj = jasmine.createSpyObj('FormManagerService', [
      'fieldData$',
      'currentSectionIndex',
      'isCompleted'
    ]);

    TestBed.configureTestingModule({
      declarations: [BaseFormComponent],
      providers: [
        { provide: FormManagerService, useValue: formManagerServiceSpyObj },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(BaseFormComponent);
    component = fixture.componentInstance;

    formManagerServiceSpy = TestBed.inject(FormManagerService) as jasmine.SpyObj<FormManagerService>;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set the form name, field data$, section index, and is completed', () => {
    const fieldData = [{ id: '1', value: 'test' }];
    formManagerServiceSpy.fieldData$.and.returnValue(of(fieldData));
    formManagerServiceSpy.currentSectionIndex.and.returnValue(1);
    formManagerServiceSpy.isCompleted.and.returnValue(false);

    component.ngOnInit();

    expect(component.formName).toEqual('Form Name');
    expect(component.fieldData$).toEqual(of(fieldData));
    expect(component.sectionIndex).toEqual(1);
    expect(component.isCompleted).toEqual(false);
  });

  it('should call the form manager service to go to the next section', () => {
    component.goToNextSection();
    expect(formManagerServiceSpy.goToNextSection).toHaveBeenCalled();
  });

  it('should call the form manager service to go to the previous section', () => {
    component.goToPreviousSection();
    expect(formManagerServiceSpy.goToPreviousSection).toHaveBeenCalled();
  });

  it('should call the form manager service to go to a specific section', () => {
    const sectionId = '123';
    component.goToSection(sectionId);
    expect(formManagerServiceSpy.goToSection).toHaveBeenCalledWith(sectionId);
  });
});
