import { Component, Input } from '@angular/core';
import { ValidationErrors } from '@angular/forms';
import { Observable } from 'rxjs';
import { QuestionValidator } from '../../services/models';

@Component({
  selector: 'app-validatormessage',
  templateUrl: './validatormessage.component.html',
  styleUrls: ['./validatormessage.component.scss']
})
export class ValidatormessageComponent {
@Input() validators: QuestionValidator[] = []

@Input() errors: ValidationErrors | null = {}

@Input() showError: boolean = false

@Input() controlChanges$!: Observable<any>

}
