import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ValidatormessageComponent } from './validatormessage.component';

describe('ValidatormessageComponent', () => {
  let component: ValidatormessageComponent;
  let fixture: ComponentFixture<ValidatormessageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ValidatormessageComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ValidatormessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
