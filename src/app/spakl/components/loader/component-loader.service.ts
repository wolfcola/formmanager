import { Type } from '@angular/core';
import { InputComponent } from '../input/input.component';

export const componentMap = new Map<string, Type<any>>([
    ['input', InputComponent],
    // add additional mappings for other components as needed
  ]);

