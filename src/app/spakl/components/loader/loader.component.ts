import { AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, ComponentRef, inject, Input, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { FormManagerService } from '../../services/form-manager.service';
import { Question } from '../../services/models';
import { componentMap } from './component-loader.service';


@Component({
  selector: 'app-loader',
  template: `<ng-container  #formComponent ></ng-container>`,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoaderComponent implements OnInit {
  @Input() component!: string;


  @ViewChild('formComponent', { static: true, read: ViewContainerRef }) viewRef!: ViewContainerRef;
  componentRef!: ComponentRef<any>;


  fm = inject(FormManagerService)
  cdr = inject(ChangeDetectorRef)

  ngOnInit() {
    const config = this.fm.getQuestionConfigById(this.component)
    console.log(config)
    const componentType = componentMap.get(config.component);
    if (!componentType) {
      throw new Error(`Component type ${config.component} not supported`);
    }
    this.loadComponent(componentType);
    this.configureComponent(config)
    this.cdr.detectChanges()
  }



  loadComponent(componentType: any){
    this.clearRenderer()
    console.log("creating component")
    this.componentRef = this.viewRef.createComponent(componentType);
  }

  configureComponent(config: Question){
  this.componentRef.instance.questionConfig = config;
  }

  clearRenderer(){
    this.viewRef.clear()
  }
}
