import { Component, OnInit } from '@angular/core';
import { BaseFormComponent } from '../base/base.component';

@Component({
  selector: 'app-input',
  template: `
  <div *appShowIf="questionConfig.showIf ? questionConfig.showIf : { id: 'default', operator: 'default', value: true }">
    <mat-form-field >
      <mat-label>{{ questionConfig.text }}</mat-label>
      <input matInput [formControl]="formControl" (input)="updateValue()">
    </mat-form-field>
    <ng-container *ngIf="questionConfig.validators">
      <app-validatormessage [showError]="formControl.invalid" [errors]="formControl.errors" [validators]="questionConfig.validators" [controlChanges$]="formControl.statusChanges"></app-validatormessage>
    </ng-container>
  </div>
  `,
})
export class InputComponent extends BaseFormComponent implements OnInit {

  ngOnInit(){
    console.log("input init")
    this.initializeComponenet()
    this.setFormControl()
  }

  getErrorMessage() {
    const errors = this.formControl.errors;
    if (errors && errors['required']) {
      return 'Please select an option';
    }
    return '';
  }

  updateValue(){
    if (this.formControl.valid){
      console.log("updating value")
      this.formManagerService.updateFieldData(this.questionConfig.id, this.formControl.value)
    } else {
      this.formManagerService.updateFieldData(this.questionConfig.id, null)
    }
  }
}
