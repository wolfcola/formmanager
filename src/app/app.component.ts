import { Component, inject } from '@angular/core';
import { map, Observable } from 'rxjs';
import { FormManagerService } from './spakl/services/form-manager.service';
import { FormConfiguration, FormState } from './spakl/services/models';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'formManager';
  fm = inject(FormManagerService)
  form$!: Observable<FormConfiguration>
  activeSectionId = ""
  ngOnInit(){
    this.fm.setState({
      formName: 'init',
      formLibraryId: 'init',
      createdBy: 'alias-init',
      createdTime: '',
      modifiedBy: '',
      lastModifiedTime: '',
      organizationId: '',
      currentSectionIndex: 0,
      isCompleted: false,
      formConfiguration: {
          questions: [
            {
              id: "1",
              component: "input",
              text: "Enter 1",
              options: [
                {text:"minLength", value: 1},
                {text:"maxLength", value: 2}
              ],
              analyticId: "age",
              validators: [
                {
                  type: "required",
                  message: "Please enter your age."
                },
                {
                  type: "minLength",
                  message: "Please enter a minimum length of 1."
                },
                {
                  type: "maxLength",
                  message: "Please enter a maximum length of 2."
                }
              ]
            },
            {
              id: "2",
              component: "input",
              text: "Enter your age",
              options: [
                {text:"minLength", value: 1},
                {text:"maxLength", value: 2}
              ],
              analyticId: "age",
              validators: [
                {
                  type: "required",
                  message: "Please enter your age."
                },
                {
                  type: "minLength",
                  message: "Please enter a minimum length of 1."
                },
                {
                  type: "maxLength",
                  message: "Please enter a maximum length of 2."
                }
              ]

            },
            {
              id: "3",
              component: "input",
              text: "Enter your email address",
              analyticId: "email",
              options: [],
              showIf: {
                id: "1",
                operator: '>=',
                value: "1"
              },
              validators: [
                {
                  type: "required",
                  message: "Please enter your email address."
                },
                {
                  type: "email",
                  message: "Please enter a valid email address."
                }
              ]
            },
          ],
          sections: [
            {
              id: "1",
              name: "Basic Information",
              analyticId: "test",
              questions: ["1"]
            },
            {
              id: "2",
              name: "Basic Information",
              analyticId: "test",
              questions: ["2","3"]
            }
          ]
      },
      fieldData: []
    })
    this.form$ = this.fm.state$.pipe(map((state: FormState)=> {return state.formConfiguration}))
    this.activeSectionId = this.fm.currentSectionId
  }

  updateId(){
    if (this.activeSectionId === "1"){
      this.activeSectionId = "2"
    }
    else {
      this.activeSectionId = "1"
    }
  }
}
